let skillDOM = document.getElementById("skill-lists");

const SkillLists = [{
    img:"../iconfinder_React.js_logo_1174949.svg",
    name:"ReactJS"
},{
    img:"../iconfinder_java_386470.svg",
    name:"Java"
},{
    img:"../iconfinder_js_282802.svg",
    name:"JavaScript"
},{
    img:"../iconfinder_267_Python_logo_4375050.svg",
    name:"Python"
},{
    img:"../ruby-seeklogo.com.svg",
    name:"Ruby"
},{
    img:"../django-seeklogo.com.svg",
    name:"Django"
},{
    img:"../Play.svg",
    name:"Play"
},{
    img:"../iconfinder_144_Gitlab_logo_logos_4373151.svg",
    name:"Gitlab"
}];

skillDOM.innerHTML += /*html*/`
    ${SkillLists.map((obj,index) => {
        return /*html*/`
            <div class="mx-3 my-2" style="display: block;" >
                <div class="text-center">
                    <img src=${obj.img} alt="" class="img-fluid" style="max-height: 100px;"/>
                    <h3 class="bold align-self-center mt-3">${obj.name}</h3>
                </div>
            </div>
        `
    }).join("")}
`