let projectDOM = document.getElementById("project-content");

const ProjectList = [{
    image:"../Imaba.svg",
    title:"iMaba",
    year:"2019",
    desc:"This is a web for new students in my faculty ( Computer Science ). It contains many information about lectures and culture in my faculty.",
    repo:"https://github.com/hsjsjsj009/IMABA",
    web:"https://bem.cs.ui.ac.id/imaba/"
},{
    image:"../SEAPay.svg",
    title:"SEA Pay",
    year:"2019",
    desc:"This is a project that i created in Software Engineering Academy COMPFEST. This is a payment app and marketplace simulator. It can pay, buy, and use some voucher. And this app support 3 role in the system, that are Customer, Merchant, and Admin.",
    repo:"https://gitlab.com/andraantariksa/SEAPay"
}]

projectDOM.innerHTML += `
    ${ProjectList.map(obj => {
        return /*html*/`
            <div class="col-4 mx-auto" style="min-width:350px;">
                <div class="row">
                    <div class="col px-0">
                        <div class="row project-detail justify-content-start">
                            <img src="${obj.image}" alt="" class="align-self-center col-6" style="max-height:${obj.title === "SEA Pay" ? "150px" : "80px"}; max-width:auto;"/>
                            <div class="col-4 px-0 align-self-center">
                                <h3 class="bold">
                                    ${obj.title}
                                </h3>
                                <h4>${obj.year}</h4>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <p class="text-justify">
                            Description : <br/>${obj.desc}
                        </p>
                        <div class="mx-0" style="width:100%">
                            <p class="bold no-style">
                                <a href="${obj.repo}" class="float-left" target="_blank">Repository Here!</a>
                                ${obj.web !== undefined ? `<a href="${obj.web}" target="_blank" class="float-right">Go To The Website!</a>` : ""}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        `
    }).join("")}
`

